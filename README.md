# Birthdates-Store-App

Birthday Store App is terminal based application developed in [NodeJS](https://nodejs.org/en/). In this application user can add a friend with his date of birth, can delete and can list all friends with their respective birth dates. I have also used [Moment.js](https://momentjs.com) to format date of births and to show current age.

#### Commands with Demo Images:

( Run commands in terminal )

##### Add a new friend

`node app.js add --name="Shashi Kant Yadav" --dob="07-12-1998"`

![Add Friend](/Images/addFriend.png)

If name already present, It will show error

![Already Added](/Images/alreadyAdded.png)

##### Remove a friend

`node app.js remove --name="Shashi Kant Yadav"`

![Delete Friend](/Images/deleteFriend.png)

If name is not present in JSON file, then it will show error

![not Found](/Images/notFoundToDelete.png)

##### Read a friend's birthday

`node app.js read --name="Shashi Kant Yadav"`

![Read Friend](/Images/readFriend.png)

If name not found then it will show an error

![not Found](/Images/notFoundToRead.png)

##### List all friends's birthday

`node app.js list`

![List Friend](/Images/listFriend.png)

If No friend in JSON File, then it will show something like this

![Empty Data](/Images/emptyDatabase.png)

##### Sort all friends's birthdate

`node app.js sort`

![Sort BirthDates](/Images/sortFriend.png)

If No birthdate in JSON File, then it will show something like this

![Not Found](/Images/notFoundBday.png)

##### Get latest single upcoming birthdate

`node app.js latest`

![Latest](/Images/latest.png)

##### About Developer

`node app.js developer`

![About Developer](/Images/aboutDev.png)

<p align="center">
  <img src="./Images/aboutDev.png">
</p>

##### Yargs Version

`node app.js --version`

![App Version](/Images/version.png)

##### Help Command

`node app.js --help`

![Help Command](/Images/helpCommand.png)

If you need help in specific command. eg: need help in `add` command -

`node app.js add --help`

![Help In Add](/Images/helpForAddCommand.png)

### Built With :

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [NodeJS](https://nodejs.org/en/)
- [Chalk](https://www.npmjs.com/package/chalk)
- [Yargs](https://www.npmjs.com/package/yargs)
- [Moment.js](https://www.npmjs.com/package/moment)

### Developed By :

- [Shashi Kant Yadav](https://bitbucket.org/{5374df3f-07c6-4a04-aa51-3e2f5760fa6a})

### License :

- [ISC License](https://choosealicense.com/licenses/isc/)
